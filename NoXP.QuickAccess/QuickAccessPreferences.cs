﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    public enum AssetTypes
    {
        GameObject = 1,
        Texture2D = 2,
        Sprite = 4,
        Cubemap = 8,
        Mesh = 16,
        Material = 32,
        ProceduralMaterial = 64,
        Shader = 128,
        AnimationClip = 256,
        TextAsset = 512,
        AudioClip = 1024,
        Script = 2048,
        DefaultAsset = 4096,
        Other = 1073741824
    }

    public class QuickAccessPreferences
    {
        private const string KeyName_TypeFilter = "NoXP.QuickAccess.Preferences.TypeFilter";
        private const string KeyName_BorderThickness = "NoXP.QuickAccess.Preferences.BorderThickness";
        private const string KeyName_BorderColorR = "NoXP.QuickAccess.Preferences.BorderColorR";
        private const string KeyName_BorderColorG = "NoXP.QuickAccess.Preferences.BorderColorG";
        private const string KeyName_BorderColorB = "NoXP.QuickAccess.Preferences.BorderColorB";


        private static bool _loaded = false;
        private static bool _foldoutAssetFilters = true;

        private static float _borderThickness = 1.0f;
        public static float BorderThickness
        { get { return _borderThickness; } }

        private static Color32 _borderColor = new Color32(0, 122, 204, 255);
        public static Color BorderColor
        { get { return _borderColor; } }

        private static AssetTypes _typeFilter = (AssetTypes)int.MaxValue; // -1 = all
        public static AssetTypes TypeFilter
        { get { return _typeFilter; } }




        [PreferenceItem("Quick Access")]
        public static void PreferencesGUI()
        {
            float labelWidth;
            float fieldWidth;
            // Load the preferences
            if (!_loaded)
            {
                _typeFilter = (AssetTypes)EditorPrefs.GetInt(KeyName_TypeFilter, int.MaxValue);
                _borderThickness = EditorPrefs.GetFloat(KeyName_BorderThickness, 1.0f);
                _borderColor.r = (byte)EditorPrefs.GetInt(KeyName_BorderColorR, 0);
                _borderColor.g = (byte)EditorPrefs.GetInt(KeyName_BorderColorG, 122);
                _borderColor.b = (byte)EditorPrefs.GetInt(KeyName_BorderColorB, 204);
                _loaded = true;
            }

            GUILayout.Label("Look & Feel", EditorStyles.whiteLargeLabel);
            labelWidth = EditorGUIUtility.labelWidth;
            EditorGUI.indentLevel++;
            EditorGUIUtility.labelWidth = 130.0f;
            _borderThickness = EditorGUILayout.Slider("Border Thickness", _borderThickness, 0, 1.0f);
            using (new EditorGUILayout.HorizontalScope())
            {
                _borderColor = EditorGUILayout.ColorField("Border Color", _borderColor);
                if (GUILayout.Button("Reset", EditorStyles.miniButton, GUILayout.ExpandWidth(false)))
                    _borderColor = new Color32(0, 122, 204, 255);
            }
            EditorGUIUtility.labelWidth = labelWidth;
            EditorGUI.indentLevel--;

            EditorGUILayout.Separator();

            GUILayout.Label("Search", EditorStyles.whiteLargeLabel);
            _foldoutAssetFilters = EditorGUILayout.Foldout(_foldoutAssetFilters, "Asset Filter");
            if (_foldoutAssetFilters)
            {
                EditorGUI.indentLevel++;
                labelWidth = EditorGUIUtility.labelWidth;
                fieldWidth = EditorGUIUtility.fieldWidth;
                EditorGUIUtility.labelWidth = 130.0f;
                EditorGUIUtility.fieldWidth = 30.0f;


                _typeFilter = MaskFilterType("All", _typeFilter, (AssetTypes)int.MaxValue);

                Array arrValues = Enum.GetValues(typeof(AssetTypes));
                for (int i = 0; i < arrValues.Length; i += 2)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (i < arrValues.Length)
                            _typeFilter = MaskFilterType(_typeFilter, (AssetTypes)arrValues.GetValue(i));
                        if ((i + 1) < arrValues.Length)
                            _typeFilter = MaskFilterType(_typeFilter, (AssetTypes)arrValues.GetValue(i + 1));
                    }
                }
                EditorGUIUtility.labelWidth = labelWidth;
                EditorGUIUtility.fieldWidth = fieldWidth;
                EditorGUI.indentLevel--;
            }


            // Save the preferences
            if (GUI.changed)
            {
                EditorPrefs.SetInt(KeyName_TypeFilter, (int)_typeFilter);
                EditorPrefs.SetFloat(KeyName_BorderThickness, _borderThickness);
                EditorPrefs.SetFloat(KeyName_BorderColorR, _borderColor.r);
                EditorPrefs.SetFloat(KeyName_BorderColorG, _borderColor.g);
                EditorPrefs.SetFloat(KeyName_BorderColorB, _borderColor.b);
            }
        }

        private static AssetTypes MaskFilterType(string label, AssetTypes filter, AssetTypes mask)
        {
            bool prevValue = (filter & mask) == mask;
            bool toggle = EditorGUILayout.Toggle(label, prevValue);
            if (prevValue != toggle)
            {
                if (!toggle)
                    filter &= ~mask;
                else
                    filter |= mask;
            }
            return filter;
        }
        private static AssetTypes MaskFilterType(AssetTypes filter, AssetTypes mask)
        {
            return MaskFilterType(mask.ToString(), filter, mask);
        }

        public static bool BitMask(AssetTypes filter, AssetTypes mask)
        { return (filter & mask) == mask; }

    }

}