﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    public partial class QuickAccess : EditorWindow
    {
        public enum QuickAccessCommand
        {
            None,
            Update, // used to update internal lists and filters
            Search, // execute the actual search on the current list
        }

        public enum QuickAccessFilterMode
        {
            Default, // when no mode shortcode is used
            Exact
        }

        private const string ControlID_SearchField = "NoXP.QuickAccess.Search";

        private const int Navigation_PageSize = 10; // number of entries stepped over when using the PageUp/PageDown keys
        private const int Navigation_PageMaxSize = 250; // maximum number of displayed items
        private const int ItemIconWidth = 24;
        private const int ItemOptionWidth = 18;


        // filter tags for various types (methods, components, gameobjects, etc.)
        public const string FilterTagComponent = "c:";
        public const string FilterTagGameObject = "g:";
        public const string FilterTagMethod = "f:";
        public const string FilterTagMenuItem = "m:";
        public const string FilterTagScript = "s:";


        // the main filter tag to filter results for the given type
        public const string FilterTagType = "t:";
        // object type tags to filter the project for
        public const string TypeTagSprite = "sprite";
        public const string TypeTagTexture = "tex";
        public const string TypeTagTexture2D = "tex2D";
        public const string TypeTagTexture3D = "tex3D";
        public const string TypeTagMaterial = "mat";
        public const string TypeTagShader = "shader";
        public const string TypeTagMesh = "mesh";
        public const string TypeTagGameObject = "go";
        public const string TypeTagFolder = "dir";


        private static float WindowWidth
        { get { return 200 + WindowWidthExtends * 5; } }

        private static int _windowWidthExtends = 0;
        private static int WindowWidthExtends
        { get { return Mathf.Clamp(_windowWidthExtends, 0, 20); } }

        private static QuickAccess _instance = null;


        [MenuItem("NoXP/QuickAccess #&q", false, 0)]
        public static void ShowQuickAccess()
        {

            if (_instance != null)
            {
                _instance.Close();
                QuickAccess[] allInstances = Resources.FindObjectsOfTypeAll<QuickAccess>();
                foreach (QuickAccess inst in allInstances)
                    inst.Close();
            }
            else if (_instance == null)
            {
                QuickAccess.Database.BuildDB();

                _instance = EditorWindow.CreateInstance<QuickAccess>();
                _instance._justCreated = true;
                _instance.ShowPopup();
            }

        }

        private int _selectedIndex = 0;
        private Vector2 _scrollPos = Vector2.zero;
        private QuickAccessItemType _filterItemType = QuickAccessItemType.None;
        private QuickAccessFilterMode _filterMode = QuickAccessFilterMode.Default;
        private string[] _filterCategories = new string[0];

        private QuickAccessCommand _command = QuickAccessCommand.None;
        private static string _searchPattern = string.Empty;
        private static string _prevSearchPattern = "~";

        private bool _justCreated = true;
        private static bool _isScrollBarVisible = false;
        private static List<QuickAccessItem> _types = new List<QuickAccessItem>();
        private static List<QuickAccessItem> _items = new List<QuickAccessItem>();


        public int NumberOfItems
        { get { return _items.Count; } }



        private void OnGUI()
        {
            // get current quick item response instance
            QuickAccessItemResponse response = QuickAccessItemResponse.Current;
            response.Reset();
            Event ev = Event.current;
            _command = QuickAccessCommand.None;

            if (GUI.GetNameOfFocusedControl().Equals(QuickAccess.ControlID_SearchField) && _selectedIndex != -1)
                _selectedIndex = -1;

            float bSize = QuickAccessPreferences.BorderThickness; // border size
            EditorGUI.DrawRect(new Rect(0, 0, this.position.width, bSize), QuickAccessPreferences.BorderColor);
            EditorGUI.DrawRect(new Rect(0, 0, bSize, this.position.height), QuickAccessPreferences.BorderColor);
            EditorGUI.DrawRect(new Rect(this.position.width - bSize, 0, bSize, this.position.height), QuickAccessPreferences.BorderColor);
            EditorGUI.DrawRect(new Rect(0, this.position.height - bSize, this.position.width, bSize), QuickAccessPreferences.BorderColor);

            GUILayout.BeginArea(new Rect(bSize, bSize, this.position.width - bSize * 2, this.position.height - bSize * 2));
            {
                GUILayoutOption typeMinWidth = GUILayout.MinWidth(QuickAccess.ItemIconWidth);
                GUILayoutOption typeMaxWidth = GUILayout.MaxWidth(QuickAccess.ItemIconWidth);
                GUILayoutOption noExpandWidth = GUILayout.ExpandWidth(false);

                // upper spacing
                GUILayout.Space(2.0f);
                using (new EditorGUILayout.HorizontalScope(GUILayout.MinHeight(20.0f)))
                {
                    GUILayout.Space(2.0f);
                    GUI.SetNextControlName(QuickAccess.ControlID_SearchField);
                    //_searchPattern = GUILayout.TextField(_searchPattern);
                    _searchPattern = EditorGUILayout.TextField(_searchPattern, EditorStyles.toolbarTextField, GUILayout.ExpandWidth(true));
                    // Standard Assets/Characters/ThirdPersonCharacter/Textures/Ethan
                    GUILayout.Space(2.0f);
                }
                GUILayout.Space(2.0f);
                using (EditorGUILayout.ScrollViewScope svs = new EditorGUILayout.ScrollViewScope(_scrollPos, GUILayout.Width(QuickAccess.WindowWidth - bSize * 2)))
                {
                    using (new EditorGUILayout.VerticalScope())
                    {
                        _scrollPos = svs.scrollPosition;
                        for (int i = 0; i < this.NumberOfItems; i++)
                        {
                            QuickAccessItem item = _items[i];
                            Rect selectRect;
                            using (EditorGUILayout.HorizontalScope hScope = new EditorGUILayout.HorizontalScope())
                            {
                                selectRect = hScope.rect;
                                GUILayout.Label(GetItemTypeContent(item), QuickAccessStyles.StyleItemCenter, typeMinWidth, typeMaxWidth);
                                QuickAccess.DrawItem(item, ref response);
                            }

                            if (_selectedIndex == i)
                                EditorGUI.DrawRect(selectRect, QuickAccessStyles.ColorItemSelected);
                        }
                    }
                }
                if (this.NumberOfItems == 0)
                    GUILayout.Label("No matches found.");
            }
            GUILayout.EndArea();

            if (!_justCreated)
            {
                // process key inputs
                this.HandleKeyEvents(ev, ref response);

                // process commands
                if (_command == QuickAccessCommand.Search)
                {
                    GUI.FocusControl("");
                    this.RefreshItems(_searchPattern);
                    _searchPattern = string.Empty;
                    this.Repaint();
                }

                // Deferred UI Event Handling
                #region Deferred UI Event Handling (QuickAccessItemResponse)
                if (response.Triggered && response.Item != null)
                    this.HandleDeferredUIEvent(ref response);
                #endregion
            }

            if (_justCreated)
            {
                _justCreated = false;
                this.RefreshSize(ev.mousePosition);
                this.ShowPopup();
                this.Focus();
                EditorGUI.FocusTextInControl(QuickAccess.ControlID_SearchField);
            }
        }

        public static void DrawItem(QuickAccessItem item, ref QuickAccessItemResponse response)
        {
            int options = 0;
            if (!item.Guid.Equals(Guid.Empty)) options++;
            float maxWidth = QuickAccess.WindowWidth - (QuickAccessPreferences.BorderThickness * 2) - QuickAccess.ItemIconWidth - options * QuickAccess.ItemOptionWidth;
            if (_isScrollBarVisible)
                maxWidth -= 16;
            GUILayoutOption maxWidthItem = GUILayout.MaxWidth(maxWidth);

            if (item.SelectValidate())
                if (GUILayout.Button(QuickAccessStyles.UIRES_Select, QuickAccessStyles.StyleIconCenter, GUILayout.MaxWidth(QuickAccess.ItemOptionWidth)))
                {
                    item.Select();
                }

            bool result = false;
            result = GUILayout.Button(item.Content, QuickAccessStyles.StyleItemLeft, maxWidthItem);
            if (result && !response.Triggered)
            {
                response.Triggered = true;
                response.Item = item;
                response.SetData(Event.current);
            }
        }



        private GUIContent GetItemTypeContent(QuickAccessItem item)
        {
            Texture2D thumbnail = AssetPreview.GetMiniTypeThumbnail(item.Type);
            if (thumbnail == null || thumbnail.Equals(QuickAccessStyles.BuiltIn_Icon_Object))
            {
                //if (TComponent.IsAssignableFrom(item.Type)) thumbnail = AssetPreview.GetMiniTypeThumbnail(TComponent);
                if (TGameObject.IsAssignableFrom(item.Type)) thumbnail = AssetPreview.GetMiniTypeThumbnail(TGameObject);
                if (TBuiltInAttribute.IsAssignableFrom(item.Type)) thumbnail = GetItemTypeIcon(item.ItemType);
            }
            // special case for text assets which are c# or JavaScript code files
            if (TTextAsset.IsAssignableFrom(item.Type))
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(item.Guid.ToString("N")).ToLowerInvariant();
                if (assetPath.EndsWith(".cs")) thumbnail = QuickAccessStyles.BuiltIn_Icon_CSScript;
                if (assetPath.EndsWith(".js")) thumbnail = QuickAccessStyles.BuiltIn_Icon_JSScript;
            }

            return new GUIContent(string.Empty, thumbnail, item.Type.FullName);
        }

        private Texture2D GetItemTypeIcon(QuickAccessItemType itemType)
        {
            switch (itemType)
            {
                case QuickAccessItemType.Component:
                    return QuickAccessStyles.UIRES_QABI_Component;
                case QuickAccessItemType.GameObject:
                    return AssetPreview.GetMiniTypeThumbnail(TGameObject);
                case QuickAccessItemType.Method:
                    return QuickAccessStyles.UIRES_QABI_Method;
                case QuickAccessItemType.Script:
                    return QuickAccessStyles.UIRES_QABI_Script;
                case QuickAccessItemType.MenuItem:
                    return QuickAccessStyles.UIRES_QABI_MenuItem;
                case QuickAccessItemType.Undefined:
                    return QuickAccessStyles.UIRES_QABI_Undefined;
                case QuickAccessItemType.None:
                    return QuickAccessStyles.UIRES_QABI_None;
            }
            return null;
        }


        private IEnumerable<QuickAccessItem> FilterItems(string pattern)
        {
            string searchPatternLI = pattern.ToLowerInvariant();
            switch (_filterMode)
            {
                case QuickAccessFilterMode.Exact:
                    return _types.Where(t => t.Name.Equals(pattern))
                                .OrderBy(t => t.Name);

                default:
                    return _types.OrderByDescending(t => CalculateSimilarity(searchPatternLI, t.NameLI))
                                                .ThenByDescending(t => CalculateSimilarity(searchPatternLI, t.FullNameLI))
                                                .Where(t => t.FullName.Contains(pattern) || t.NameLI.Contains(searchPatternLI))
                                                .Where(t => FilterItemType(_filterItemType, t.ItemType))
                                                .Where(t => FilterCategories(_filterCategories, t.Categories)) // filter for the categories
                                                .OrderBy(t => t.NameLI.IndexOf(searchPatternLI))
                                                .OrderBy(t => t.Name);
            }
        }
        private bool FilterCategories(string[] filters, string[] categories)
        {
            if (filters == null || filters.Length == 0)
                return true;
            else
            {
                foreach (string filter in filters)
                    if (categories.Contains(filter))
                        return true;
            }
            return false;
        }
        private bool FilterItemType(QuickAccessItemType filter, QuickAccessItemType itemType)
        {
            if (filter == QuickAccessItemType.None)
                return true;
            else
                return filter == itemType;
        }


        private string ExtractFilterMode(string pattern)
        {
            if (pattern.StartsWith("."))
            {
                _filterMode = QuickAccessFilterMode.Exact;
                return pattern.Substring(1);
            }
            else
                _filterMode = QuickAccessFilterMode.Default;
            return pattern;
        }
        private string ExtractFilterCategory(string pattern)
        {
            string[] splitted = pattern.Split(';');
            if (splitted.Length == 0)
                return pattern;
            else
            {
                Array.Resize(ref _filterCategories, splitted.Length - 1);
                for (int i = 0; i < _filterCategories.Length; i++)
                    _filterCategories[i] = splitted[i].ToLowerInvariant();
                return pattern.Substring(pattern.LastIndexOf(";") + 1);
            }

        }
        private string ExtractFilterItemType(string pattern)
        {
            string searchPatternLI = pattern.ToLowerInvariant();
            string searchPatternResult = pattern;

            if (searchPatternLI.StartsWith(QuickAccess.FilterTagGameObject))
            {
                _filterItemType = QuickAccessItemType.GameObject;
                searchPatternResult = pattern.Replace(QuickAccess.FilterTagGameObject, "");
            }
            else if (searchPatternLI.StartsWith(QuickAccess.FilterTagComponent))
            {
                _filterItemType = QuickAccessItemType.Component;
                searchPatternResult = pattern.Replace(QuickAccess.FilterTagComponent, "");
            }
            else if (searchPatternLI.StartsWith(QuickAccess.FilterTagMethod))
            {
                _filterItemType = QuickAccessItemType.Method;
                searchPatternResult = pattern.Replace(QuickAccess.FilterTagMethod, "");
            }
            else if (searchPatternLI.StartsWith(QuickAccess.FilterTagMenuItem))
            {
                _filterItemType = QuickAccessItemType.MenuItem;
                searchPatternResult = pattern.Replace(QuickAccess.FilterTagMenuItem, "");
            }
            else if (searchPatternLI.StartsWith(QuickAccess.FilterTagScript))
            {
                _filterItemType = QuickAccessItemType.Script;
                searchPatternResult = pattern.Replace(QuickAccess.FilterTagScript, "");
            }
            else
                _filterItemType = QuickAccessItemType.None;

            return searchPatternResult;
        }


        private void RefreshItems(string pattern)
        {
            if (_searchPattern.Equals(_prevSearchPattern))
                return;

            // extract shortcode stuff
            pattern = this.ExtractFilterMode(pattern);
            pattern = this.ExtractFilterCategory(pattern);
            pattern = this.ExtractFilterItemType(pattern);


            _items.Clear();
            _types.Clear();
            _prevSearchPattern = _searchPattern;
            string searchPatternLI = pattern.ToLowerInvariant();

            // get all default items from the database
            foreach (string key in QuickAccess.Database[QuickAccess.DBTypes.Default].Keys)
            {
                if (QuickAccess.Database[QuickAccess.DBTypes.Default][key].NameLI.Contains(searchPatternLI))
                    _types.Add(QuickAccess.Database[QuickAccess.DBTypes.Default][key]);
                else if (QuickAccess.Database[QuickAccess.DBTypes.Default][key].FullNameLI.Contains(searchPatternLI))
                    _types.Add(QuickAccess.Database[QuickAccess.DBTypes.Default][key]);
            }
            // get all asset items from the database
            foreach (string key in QuickAccess.Database[QuickAccess.DBTypes.Assets].Keys)
            {
                if (QuickAccess.Database[QuickAccess.DBTypes.Assets][key].NameLI.Contains(searchPatternLI))
                    _types.Add(QuickAccess.Database[QuickAccess.DBTypes.Assets][key]);
                else if (QuickAccess.Database[QuickAccess.DBTypes.Assets][key].FullNameLI.Contains(searchPatternLI))
                    _types.Add(QuickAccess.Database[QuickAccess.DBTypes.Assets][key]);
            }
            // set selection index to first entry
            if (_types.Count > 0) _selectedIndex = 0;
            else EditorGUI.FocusTextInControl(QuickAccess.ControlID_SearchField);

            // apply filter and sorting
            IEnumerable<QuickAccessItem> eTypes = this.FilterItems(pattern);

            foreach (QuickAccessItem t in eTypes.Take(QuickAccess.Navigation_PageMaxSize))
            {
                if (_filterItemType == QuickAccessItemType.None
                    || _filterItemType == t.ItemType)
                    _items.Add(t);
            }

            this.RefreshSize(); // reuse previous position
        }



        private void HandleDeferredUIEvent(ref QuickAccessItemResponse response)
        {

            //string pat = this.ExtractFilterItemType(this.ExtractFilterCategory(_searchPattern));
            //Debug.Log("'" + response.Item.NameLI + "' => '" + _searchPattern.ToLowerInvariant() + "' ... " + string.Join(";", response.Item.Categories) + ":: " + response.Item.Type.Name);

            if (response.MouseButton == 0)
            {
                if (response.Modifiers == EventModifiers.Shift)
                {
                    if (response.Item.SelectValidate())
                        response.Item.Select();
                    else
                    {
                        Debug.LogWarning("Cannot select the specified object: " + response.Item.Name);
                    }
                }

                if (response.Modifiers == EventModifiers.None
                    || response.Modifiers == EventModifiers.Control)
                {
                    // check if the QuickAccessItem has a guid defined (then it must be something from the project view
                    if (!response.Item.Guid.Equals(Guid.Empty))
                    {
                        UnityEngine.Object assetObj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(AssetDatabase.GUIDToAssetPath(response.Item.Guid.ToString("N")));
                        Type t = assetObj.GetType();
                        if (t.Equals(QuickAccess.TGameObject))
                        {
                            GameObject go = GameObject.Instantiate<GameObject>((GameObject)assetObj);
                            go.transform.position = SceneView.lastActiveSceneView.pivot;
                            Undo.RegisterCreatedObjectUndo(go, "Quick Access - Execute");
                            Selection.activeGameObject = go;
                        }
                        else
                        {
                            // general case, open all other asset types (using the default Unity action for that type)
                            AssetDatabase.OpenAsset(assetObj, 0); // the line number is ignored when the IDE is already opened
                        }

                    }
                    else if (QuickAccess.TComponent.IsAssignableFrom(response.Item.Type))
                    {
                        if (Selection.activeGameObject == null)
                        {
                            Selection.activeGameObject = new GameObject(GameObjectUtility.GetUniqueNameForSibling(null, "GameObject"));
                            Undo.RegisterCreatedObjectUndo(Selection.activeGameObject, "Quick Access - Execute");
                        }
                        // add the specific component to each selected GameObject
                        if (Selection.gameObjects.Length != 0)
                            foreach (GameObject go in Selection.gameObjects)
                                Undo.AddComponent(go, response.Item.Type);
                    }
                    else
                    {
                        // general case which assumes all others are defined using BuiltInAttributes
                        //  ->  needs to be changed when additional ways exist (perhaps for parameterized invokations)
                        ((BuiltInAttribute)response.Item.Object).Method.Invoke(null, null);
                    }

                }
                // set index and unfocus textfield
                _selectedIndex = _items.IndexOf(response.Item);
                GUI.FocusControl("");
            }
        }
        private void HandleKeyEvents(Event ev, ref QuickAccessItemResponse response)
        {
            // process key inputs
            if (ev.type == EventType.KeyDown)
            {
                if (ev.keyCode == KeyCode.DownArrow) this.NavigateResultsDown(1, -1, this.NumberOfItems - 1);
                if (ev.keyCode == KeyCode.UpArrow) this.NavigateResultsUp(1, -1, this.NumberOfItems - 1);
                if (ev.keyCode == KeyCode.PageDown) this.NavigateResultsDown(QuickAccess.Navigation_PageSize, -1, this.NumberOfItems - 1);
                if (ev.keyCode == KeyCode.PageUp) this.NavigateResultsUp(QuickAccess.Navigation_PageSize, -1, this.NumberOfItems - 1);
            }
            else if (ev.type == EventType.KeyUp)
            {
                // handle key up events when textfield has focus
                if (GUI.GetNameOfFocusedControl().Equals(QuickAccess.ControlID_SearchField))
                {
                    if (ev.keyCode == KeyCode.Return) _command = QuickAccessCommand.Search;
                    if (ev.keyCode == KeyCode.DownArrow) this.NavigateResultsDown();
                    if (ev.keyCode == KeyCode.PageDown) this.NavigateResultsDown(QuickAccess.Navigation_PageSize, -1, this.NumberOfItems - 1);
                    //if (ev.keyCode != KeyCode.Return && ev.keyCode != KeyCode.DownArrow && ev.keyCode != KeyCode.PageDown)
                    if ((int)ev.keyCode >= 32 && (int)ev.keyCode <= 272
                        || ev.keyCode == KeyCode.Backspace)
                    {
                        this.RefreshItems(_searchPattern);
                        this.RefreshSize();
                        this.Repaint();
                    }
                }
                // handle key up events when textfield has explicit no focus
                else
                {
                    if (ev.keyCode == KeyCode.Return
                        && _selectedIndex > -1 && _selectedIndex < this.NumberOfItems
                        && !response.Triggered)
                    {
                        response.Triggered = true;
                        response.Item = _items[_selectedIndex];
                        response.SetData(0, ev.modifiers, ev.clickCount);

                        // check if ctrl is held and close search afterwards
                        if ((ev.modifiers & EventModifiers.Control) == EventModifiers.Control)
                            this.Close();
                    }

                }

                // handle key up events regardless of focues control
                if (ev.keyCode == KeyCode.End) this.NavigateEnd();
                if (ev.keyCode == KeyCode.Home) this.NavigateHome();
                if (ev.keyCode == KeyCode.F1) PopupWindow.Show(new Rect(0, 24, 1, 1), QuickAccessHelp.Instance);

                if (ev.keyCode == KeyCode.Escape)
                {
                    if (_searchPattern.Equals(string.Empty))
                        this.Close();
                    else
                    {
                        this.ClearSearch();
                        this.Repaint();
                    }
                }

                if (GUI.changed)
                {
                    int newWindowWidthExtends = (int)(EditorStyles.toolbarTextField.CalcSize(new GUIContent(_searchPattern)).x / 5.0f) - 20;
                    if (newWindowWidthExtends != _windowWidthExtends)
                    {
                        _windowWidthExtends = newWindowWidthExtends;
                        this.RefreshSize();
                        this.Repaint();
                    }
                }
            }

        }

        private void NavigateHome()
        {
            int prevIndex = _selectedIndex;
            if (_selectedIndex > 0 && this.NumberOfItems > 0)
                _selectedIndex = 0;
            else
                _selectedIndex = -1;

            _selectedIndex = Mathf.Clamp(_selectedIndex, -1, this.NumberOfItems - 1);
            _scrollPos.y = Mathf.Clamp(_selectedIndex - 10, 0, this.NumberOfItems) * QuickAccessStyles.StyleItemCenter.CalcHeight(GUIContent.none, QuickAccess.WindowWidth);
            // focus textfield when pressed "Up" on first item
            if (_selectedIndex == -1 && prevIndex != -1)
                EditorGUI.FocusTextInControl(QuickAccess.ControlID_SearchField);
            this.Repaint();
        }
        private void NavigateEnd()
        {
            int prevIndex = _selectedIndex;
            if (_selectedIndex > -1 && this.NumberOfItems > 0)
                _selectedIndex = this.NumberOfItems - 1;
            //else
            //    _selectedIndex = 0;

            _selectedIndex = Mathf.Clamp(_selectedIndex, -1, this.NumberOfItems - 1);
            _scrollPos.y = Mathf.Clamp(_selectedIndex - 10, 0, this.NumberOfItems) * QuickAccessStyles.StyleItemCenter.CalcHeight(GUIContent.none, QuickAccess.WindowWidth);
            // unfocus textfield when pressed "Down" in textfield
            if (_selectedIndex != -1 && prevIndex == -1)
                GUI.FocusControl("");
            this.Repaint();
        }
        private void NavigateResultsDown(int step = 1, int clampLow = -1, int clampHigh = 0)
        {
            int prevIndex = _selectedIndex;
            _selectedIndex = Mathf.Clamp(_selectedIndex + step, clampLow, clampHigh);
            _scrollPos.y = Mathf.Clamp(_selectedIndex - 10, 0, this.NumberOfItems) * QuickAccessStyles.StyleItemCenter.CalcHeight(GUIContent.none, QuickAccess.WindowWidth);
            // unfocus textfield when pressed "Down" in textfield
            if (_selectedIndex != -1 && prevIndex == -1)
                GUI.FocusControl("");
            this.Repaint();
        }
        private void NavigateResultsUp(int step = 1, int clampLow = -1, int clampHigh = 0)
        {
            int prevIndex = _selectedIndex;
            _selectedIndex = Mathf.Clamp(_selectedIndex - step, clampLow, clampHigh);
            _scrollPos.y = Mathf.Clamp(_selectedIndex - 10, 0, this.NumberOfItems) * QuickAccessStyles.StyleItemCenter.CalcHeight(GUIContent.none, QuickAccess.WindowWidth);
            // focus textfield when pressed "Up" on first item
            if (_selectedIndex == -1 && prevIndex != -1)
                EditorGUI.FocusTextInControl(QuickAccess.ControlID_SearchField);

            this.Repaint();
        }


        private void RefreshSize(Vector2 pos)
        {
            Rect position = new Rect(pos, new Vector2(QuickAccess.WindowWidth, 10));
            position.height = Mathf.Max(0, Mathf.Min(20, this.NumberOfItems)) * QuickAccessStyles.StyleItemLeft.CalcHeight(new GUIContent(" "), QuickAccess.WindowWidth);
            position.height += this.NumberOfItems == 0 ? 44 : 44;
            _isScrollBarVisible = this.NumberOfItems > 20;
            this.maxSize = position.size;
            this.minSize = position.size;
            this.position = position;
        }
        private void RefreshSize()
        {
            Rect position = this.position;
            position.width = QuickAccess.WindowWidth;
            position.height = Mathf.Max(0, Mathf.Min(20, this.NumberOfItems)) * QuickAccessStyles.StyleItemLeft.CalcHeight(new GUIContent(" "), QuickAccess.WindowWidth);
            position.height += this.NumberOfItems == 0 ? 44 : 44;
            _isScrollBarVisible = this.NumberOfItems > 20;
            this.maxSize = position.size;
            this.minSize = position.size;
            this.position = position;
        }
        private void ClearSearch()
        {
            _items.Clear();
            _searchPattern = string.Empty;
            _prevSearchPattern = "~";
            _selectedIndex = -1;
            this.RefreshSize();
            EditorGUI.FocusTextInControl(QuickAccess.ControlID_SearchField);
        }


        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        public static int LevenshteinDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
                return m;

            if (m == 0)
                return n;

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            { }

            for (int j = 0; j <= m; d[0, j] = j++)
            { }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        /// <summary>
        /// Returns the number of steps required to transform the source string
        /// into the target string.
        /// </summary>
        private static int ComputeLevenshteinDistance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            // Step 1
            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

        public static double CalculateSimilarity(string s, string t)
        {
            if ((s == null) || (t == null)) return 0.0;
            if ((s.Length == 0) || (t.Length == 0)) return 0.0;
            if (s == t) return 1.0;

            int stepsToSame = ComputeLevenshteinDistance(s, t);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(s.Length, t.Length)));
        }

        // used to autoclose when clicking outside the QuickAccess window
        //void OnLostFocus()
        //{
        //    this.Close();
        //}

        void OnSelectionChange()
        {
            this.Repaint();
        }

        void OnDestroy()
        {
            if (_instance == this)
                _instance = null;
        }


        public static bool HasFlags(int value, int flags)
        { return (value & flags) == flags; }


    }


    public class QuickAccessHelp : PopupWindowContent
    {
        private static QuickAccessHelp _instance = null;
        public static QuickAccessHelp Instance
        {
            get
            {
                if (_instance == null) _instance = new QuickAccessHelp();
                return _instance;
            }
        }

        private QuickAccessHelp()
        { }

        public override void OnGUI(Rect rect)
        {
            GUILayout.BeginArea(new Rect(5, 5, rect.width - 10, rect.height - 10));
            using (new EditorGUILayout.VerticalScope())
            {
                // display the various help text entries!
                GUILayout.Label("Filters:", EditorStyles.boldLabel);
                EditorGUILayout.HelpBox("'c:' - use Component filter" + Environment.NewLine
                                      + "'g:' - use GameObject filter" + Environment.NewLine
                                      + "'f:' - use Function filter" + Environment.NewLine
                                      + "'m:' - use Menu Items filter" + Environment.NewLine
                                      + "'s:' - use Script filter", MessageType.None);
                GUILayout.Label("Categories:", EditorStyles.boldLabel);
                EditorGUILayout.HelpBox("e.g.: 'c;' or 'g;' for component or gameobject categories." + Environment.NewLine
                                      + "Use any category name and separate them with ';'", MessageType.None);
            }
            GUILayout.EndArea();
        }

    }

}