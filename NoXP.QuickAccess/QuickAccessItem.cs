﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    // ! ! ! ! 
    // strip down this class
    //  ->  everything necessary should be possible using the GuidType class directly


    public class QuickAccessItem
    {
        private GUIContent _guiContent = new GUIContent();
        private Type _innerType = null;
        private Guid _innerGuid = Guid.Empty;
        private string _name = string.Empty;
        private string _nameLI = string.Empty;
        private string _fullName = string.Empty;
        private string _fullNameLI = string.Empty;
        private object _object = null;
        private QuickAccessItemType _itemType = QuickAccessItemType.None;
        private string[] _categories = new string[0];

        // properties
        public Type Type
        { get { return _innerType; } }
        public Guid Guid
        { get { return _innerGuid; } }
        public string Name
        { get { return _name; } }
        public string NameLI
        { get { return _nameLI; } }
        public string FullName
        { get { return _fullName; } }
        public string FullNameLI
        { get { return _fullNameLI; } }
        public object Object
        { get { return _object; } }
        public QuickAccessItemType ItemType
        { get { return _itemType; } }
        public string[] Categories
        { get { return _categories; } }

        public GUIContent Content
        { get { return _guiContent; } }


        public QuickAccessItem(Type type, Guid guid, string overrideName = "")
        {
            _innerType = type;
            _innerGuid = guid;
            _name = type.Name;
            if (!overrideName.Equals(string.Empty))
                _name = overrideName;
            _fullName = type.FullName;
            _itemType = QuickAccessItem.GetItemType(type);
            _categories = QuickAccessItem.GetCategories(type);
            // check for special types: GameObject, MonoScript
            if (type.Equals(QuickAccess.TGameObject)
                || Array.IndexOf(QuickAccess.TAssetTypes, type) != -1)
            {
                string strGuid = guid.ToString("N");
                string assetPath = string.Empty;
                assetPath = AssetDatabase.GUIDToAssetPath(strGuid);
                // either assign the overrideName or the filename (without extension) to the _name field
                _name = !overrideName.Equals(string.Empty) ? overrideName : Path.GetFileNameWithoutExtension(assetPath);
                _fullName = Path.Combine(assetPath.Replace(Path.GetFileName(assetPath), string.Empty), _name);
            }
            this.SetGUIContent(_name, _fullName);
            _nameLI = _name.ToLowerInvariant();
            _fullNameLI = _fullName.ToLowerInvariant();
        }
        public QuickAccessItem(BuiltInAttribute builtin)
        {
            _innerType = QuickAccess.TBuiltInAttribute;
            _innerGuid = Guid.Empty;
            _name = builtin.Name;
            _fullName = builtin.FullName;
            _object = builtin;
            _itemType = builtin.ItemType;
            _categories = builtin.Categories;
            for (int i = 0; i < _categories.Length; i++)
                _categories[i] = _categories[i].ToLowerInvariant();
            this.SetGUIContent(_name, _fullName);
            _nameLI = _name.ToLowerInvariant();
            _fullNameLI = _fullName.ToLowerInvariant();
        }
        public QuickAccessItem(Type type, string guid, string overrideName = "") : this(type, new Guid(guid), overrideName)
        { }

        private void SetGUIContent(string name, string fullName)
        {
            // trim type name from the fullname and remove trailing '.'
            fullName = fullName.Replace(name, string.Empty).TrimEnd('.', '/');
            _guiContent.tooltip = fullName;

            if (name.Length > 26)
                name = name.Substring(0, 24) + "..";
            if (fullName.Length > 26)
                fullName = ".." + fullName.Substring(fullName.Length - 20, 20);
            _guiContent.text = string.Format("{0} <size=8><color=#888888FF>{1}</color></size>", name, fullName);
        }

        private static QuickAccessItemType GetItemType(Type type)
        {
            if (QuickAccess.TGameObject.IsAssignableFrom(type))
                return QuickAccessItemType.GameObject;
            else if (QuickAccess.TComponent.IsAssignableFrom(type))
                return QuickAccessItemType.Component;
            else if (QuickAccess.TBuiltInAttribute.IsAssignableFrom(type))
                return QuickAccessItemType.Method;
            else if (QuickAccess.TMonoScript.IsAssignableFrom(type))
                return QuickAccessItemType.Script;
            return QuickAccessItemType.Undefined;
        }

        // move to different class (might not only be used by the QuickAccessItem class
        public static string[] GetCategories(Type type)
        {
            List<string> categories = new List<string>();
            if (QuickAccess.TGameObject.IsAssignableFrom(type))
                categories.Add(BuiltIn.CatGameObject.ToLowerInvariant());
            else if (QuickAccess.TComponent.IsAssignableFrom(type))
                categories.Add(BuiltIn.CatComponent.ToLowerInvariant());
            else if (QuickAccess.TMonoScript.IsAssignableFrom(type))
                categories.Add(BuiltIn.CatScript.ToLowerInvariant());
            else if (QuickAccess.TSprite.IsAssignableFrom(type))
                categories.Add("sprite");
            else if (QuickAccess.TTexture2D.IsAssignableFrom(type))
                categories.Add("tex2d");
            else if (QuickAccess.TCubemap.IsAssignableFrom(type))
                categories.Add("cubemap");
            else if (QuickAccess.TDefaultAsset.IsAssignableFrom(type))
                categories.Add("dir");
            return categories.ToArray();
        }



        public override string ToString()
        {
            return base.ToString() + string.Format(";Name:{0}", _innerType.Name);
        }

        public bool SelectValidate()
        { return QuickAccessItem.SelectValidate(this); }
        public void Select()
        { QuickAccessItem.Select(this); }
        public static bool SelectValidate(QuickAccessItem item)
        { return (!item.Guid.Equals(Guid.Empty)); }
        public static void Select(QuickAccessItem item)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(item.Guid.ToString("N"));
            if (!item.Type.Equals(QuickAccess.TSprite))
            {
                Selection.activeObject = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
            }
            else
            {
                foreach (UnityEngine.Object obj in AssetDatabase.LoadAllAssetsAtPath(assetPath))
                {
                    if (obj.GetType().Equals(QuickAccess.TSprite)
                        && obj.name.Equals(item.Name))
                    {
                        Selection.activeObject = obj;
                        return;
                    }
                }
            }
        }


    }



    public struct QuickAccessItemResponse
    {
        // empty response 
        public static QuickAccessItemResponse Empty
        { get { return new QuickAccessItemResponse(null, false, -1, EventModifiers.None); } }

        public static QuickAccessItemResponse Current = QuickAccessItemResponse.Empty;

        private QuickAccessItem _item;
        private bool _triggered;
        private int _mouseButton;
        private EventModifiers _modifiers;
        private int _clickCount;

        public QuickAccessItem Item
        {
            get { return _item; }
            set { _item = value; }
        }
        public bool Triggered
        {
            get { return _triggered; }
            set { _triggered = value; }
        }
        public int MouseButton
        {
            get { return _mouseButton; }
            //set { _mouseButton = value; }
        }
        public EventModifiers Modifiers
        {
            get { return _modifiers; }
            //set { _modifiers = value; }
        }

        public QuickAccessItemResponse(QuickAccessItem item, bool triggered, int mouseButton, EventModifiers modifiers = EventModifiers.None)
        {
            _item = item;
            _triggered = triggered;
            _mouseButton = mouseButton;
            _modifiers = modifiers;
            _clickCount = 0;
        }

        public void Reset()
        {
            _item = null;
            _triggered = false;
            _mouseButton = -1;
            _modifiers = EventModifiers.None;
            _clickCount = 0;
        }

        public void SetData(Event ev)
        {
            _mouseButton = ev.button;
            _modifiers = ev.modifiers;
            _clickCount = ev.clickCount;
        }
        public void SetData(int mousebutton, EventModifiers modifiers, int clickCount)
        {
            _mouseButton = mousebutton;
            _modifiers = modifiers;
            _clickCount = clickCount;
        }

        public override string ToString()
        {
            return "QuickAccessItemResponse: {" + this.Item + "}, MouseButton: {" + this.MouseButton + "}, Modifiers: {" + this.Modifiers + "}";
        }
    }

}