﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    public partial class QuickAccess : EditorWindow
    {
        // implementation of the database functionality and stores

        public enum DBTypes
        {
            Default,
            Assets
        }

        public static readonly Type TObject = typeof(UnityEngine.Object);
        public static readonly Type TComponent = typeof(Component);
        public static readonly Type TMonoBehaviour = typeof(MonoBehaviour);
        public static readonly Type TGameObject = typeof(GameObject);
        public static readonly Type TBuiltInAttribute = typeof(BuiltInAttribute);
        public static readonly Type TMonoScript = typeof(MonoScript);
        public static readonly Type TDefaultAsset = typeof(DefaultAsset);
        public static readonly Type TShader = typeof(Shader);
        public static readonly Type TMaterial = typeof(Material);
        public static readonly Type TProceduralMaterial = typeof(ProceduralMaterial);
        public static readonly Type TTexture2D = typeof(Texture2D);
        public static readonly Type TSprite = typeof(Sprite);
        public static readonly Type TCubemap = typeof(Cubemap);
        public static readonly Type TMesh = typeof(Mesh);
        public static readonly Type TAnimationClip = typeof(AnimationClip);
        public static readonly Type TTextAsset = typeof(TextAsset);
        public static readonly Type TAudioClip = typeof(AudioClip);



        private static Type[] _tAssetTypes = new Type[] { };
        public static Type[] TAssetTypes
        { get { return _tAssetTypes; } }

        #region Singleton-Pattern
        private static DB _database = null;
        public static DB Database
        {
            get
            {
                if (_database == null) _database = new DB();
                return _database;
            }
        }
        #endregion


        public class DB
        {
            // ! ! ! TODO ! ! ! 
            //  ->  Add option to search/filter for specific names in the project and allow to select them
            //  ->  Add option for detail info on found types (full path for gameobject/prefabs/components, preview on prefabs, type icon etc.)
            //  ->  Add options for default primitives and other stuff from the "GameObject" menu
            //  ->  Add filtering of DB rebuild to allow leaving out scans for specific types (e.g. Components, BuiltInAttributes, GameObject, etc.)
            //  ->  refine filtering for components, as Editor scripts/components are getting listed

            private static bool _needRefresh = true;
            public static bool NeedRefresh
            { set { _needRefresh = value; } }


            private Dictionary<DBTypes, Dictionary<string, QuickAccessItem>> _databases = new Dictionary<DBTypes, Dictionary<string, QuickAccessItem>>();
            public Dictionary<string, QuickAccessItem> this[DBTypes type]
            { get { return _databases[type]; } }


            public void BuildDB()
            {
                // update asset type array to match the flags set in preferences
                _tAssetTypes = UpdateAssetTypesArray();
                if (_needRefresh)
                {
                    foreach (DBTypes type in _databases.Keys)
                        _databases[type].Clear();
                    if (!_databases.ContainsKey(DBTypes.Default)) _databases.Add(DBTypes.Default, new Dictionary<string, QuickAccessItem>());
                    if (!_databases.ContainsKey(DBTypes.Assets)) _databases.Add(DBTypes.Assets, new Dictionary<string, QuickAccessItem>());

                    IEnumerable<Type> eTypes = new List<Type>(AppDomain.CurrentDomain.GetAllDerivedTypes(typeof(Component))).Distinct();
                    foreach (Type type in eTypes)
                        _databases[DBTypes.Default].Add(type.FullName, new QuickAccessItem(type, Guid.Empty));

                    IEnumerable<BuiltInAttribute> builtIns = DB.FindAllBuiltInMethods();
                    foreach (BuiltInAttribute builtIn in builtIns)
                        _databases[DBTypes.Default].Add(builtIn.FullName, new QuickAccessItem(builtIn));

                    // build asset database
                    this.BuildDBFromAssets();
                }
                _needRefresh = false;
            }

            public bool IsEventHandlerRegistered(Delegate handler)
            {
                if (EditorApplication.projectWindowChanged != null)
                    foreach (Delegate addedHandler in EditorApplication.projectWindowChanged.GetInvocationList())
                        if (addedHandler == handler)
                            return true;
                return false;
            }
            private void BuildDBFromAssets()
            {
                _databases[DBTypes.Assets].Clear();
                foreach (Type t in _tAssetTypes)
                    BuildDBFromAssetType(t);
            }
            private void BuildDBFromAssetType(Type t)
            {
                if (!t.Equals(TSprite) && !t.Equals(TTexture2D))
                    BuildDBFromAssetTypeGeneric(t);
                else
                    BuildDBFromAssetTypeTexture2D(t);
            }
            private void BuildDBFromAssetTypeGeneric(Type t)
            {
                IEnumerable<string> eGuids = AssetDatabase.FindAssets("t:" + t.Name).Distinct();
                foreach (string guid in eGuids)
                    if (!_databases[DBTypes.Assets].ContainsKey(guid))
                        _databases[DBTypes.Assets].Add(guid, new QuickAccessItem(t, guid));
            }
            private void BuildDBFromAssetTypeTexture2D(Type t)
            {
                IEnumerable<string> eGuids = AssetDatabase.FindAssets("t:" + t.Name).Distinct();
                if (t.Equals(TSprite))
                {
                    foreach (string guid in eGuids)
                    {
                        TextureImporter ti = (TextureImporter)AssetImporter.GetAtPath(AssetDatabase.GUIDToAssetPath(guid));
                        if (ti.spriteImportMode != SpriteImportMode.None)
                        {
                            foreach (UnityEngine.Object obj in AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(guid)))
                            {
                                if (obj.GetType().Equals(TSprite))
                                {
                                    string nGuid = obj.name + "@" + guid;
                                    if (!_databases[DBTypes.Assets].ContainsKey(nGuid))
                                        _databases[DBTypes.Assets].Add(nGuid, new QuickAccessItem(t, guid, obj.name));
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (string guid in eGuids)
                    {
                        TextureImporter ti = (TextureImporter)AssetImporter.GetAtPath(AssetDatabase.GUIDToAssetPath(guid));
                        if (ti.spriteImportMode == SpriteImportMode.None)
                        {
                            if (!_databases[DBTypes.Assets].ContainsKey(guid))
                                _databases[DBTypes.Assets].Add(guid, new QuickAccessItem(t, guid));
                        }
                    }
                }
            }

            private static Type[] UpdateAssetTypesArray()
            {
                AssetTypes filter = QuickAccessPreferences.TypeFilter;
                List<Type> result = new List<Type>();
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.GameObject)) result.Add(TGameObject);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Texture2D)) result.Add(TTexture2D);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Sprite)) result.Add(TSprite);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Cubemap)) result.Add(TCubemap);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Mesh)) result.Add(TMesh);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Material)) result.Add(TMaterial);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.ProceduralMaterial)) result.Add(TProceduralMaterial);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Shader)) result.Add(TShader);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.AnimationClip)) result.Add(TProceduralMaterial);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.TextAsset)) result.Add(TTextAsset);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.AudioClip)) result.Add(TAudioClip);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.Script)) result.Add(TMonoScript);
                if (QuickAccessPreferences.BitMask(filter, AssetTypes.DefaultAsset)) result.Add(TDefaultAsset);
                // need to add "all other" types which can be queried for the "Other" option flag
                //Other = 1073741824

                return result.ToArray();
            }

            private static IEnumerable<BuiltInAttribute> FindAllBuiltInMethods()
            {
                return AppDomain.CurrentDomain.GetAllMethods();
            }



        }

    }
}