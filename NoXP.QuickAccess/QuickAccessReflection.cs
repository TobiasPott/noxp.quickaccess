﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{
    public static class QuickAccessReflection
    {


        static string[] AsmFilterSystem = new string[] { "mscorlib", "System", "Mono", "nunit", "Boo", "I18N", "ICSharpCode", "UnityScript", "UnityEditor.Graphs", "UnityEditor.EditorTestsRunner", "UnityEditor.TreeEditor", "UnityEditor." };
        static string[] AsmFilterUserCode = new string[] { "mscorlib", "System", "Mono", "nunit", "Boo", "I18N", "ICSharpCode", "UnityScript", "UnityEditor.Graphs", "UnityEditor.EditorTestsRunner", "UnityEditor.TreeEditor", "UnityEngine", "UnityEditor" };


        public static Type[] GetAllDerivedTypes(this AppDomain aAppDomain, Type aType)
        {
            List<Type> results = new List<Type>();
            Assembly[] assemblies = aAppDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                // check if assembly is part of the System libraries (see AssemblyFilterSystem array)
                if (!EvalAssemblyNameFilter(assembly.FullName, AsmFilterSystem))
                {
                    Type[] types = assembly.GetTypes();
                    foreach (Type type in types)
                    {
                        // skip abstract and interface types
                        if (type.IsAbstract || type.IsInterface)
                            continue;

                        if (type.IsSubclassOf(aType))
                            results.Add(type);
                        else if (aType.IsAssignableFrom(type))
                            results.Add(type);
                    }
                }
            }
            return results.ToArray();
        }
        public static BuiltInAttribute[] GetAllMethods(this AppDomain aAppDomain)
        {
            Type tBuiltInAttribute = typeof(BuiltInAttribute);

            List<BuiltInAttribute> results = new List<BuiltInAttribute>();
            Assembly[] assemblies = aAppDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                // check if assembly is part of the System libraries (see AssemblyFilterSystem array)
                if (!EvalAssemblyNameFilter(assembly.FullName, AsmFilterUserCode))
                {
                    Type[] types = assembly.GetTypes();
                    foreach (Type type in types)
                    {
                        // skip abstract and interface types
                        if (type.IsInterface)
                            continue;

                        foreach (MethodInfo method in type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
                        {
                            BuiltInAttribute[] attrs = (BuiltInAttribute[])method.GetCustomAttributes(tBuiltInAttribute, true);
                            foreach (BuiltInAttribute attr in attrs)
                            {
                                attr.Method = method;
                                results.Add(attr);
                            }
                        }
                    }
                }
            }
            return results.ToArray();
        }


        public static void GetAttributedTypes(this AppDomain aAppDomain)
        {
            List<Type> result = new List<Type>();
            Assembly[] assemblies = aAppDomain.GetAssemblies();
            string[] referenceFilter = new string[] { "UnityEditor" };
            foreach (Assembly assembly in assemblies)
            {
                // check if assembly is part of the System libraries (see AssemblyFilterSystem array)
                if (!EvalAssemblyNameFilter(assembly.FullName, AsmFilterSystem))
                {
                    if (EvalAssemblyReferenceFilter(assembly, referenceFilter))
                    {
                        Type[] types = assembly.GetTypes();
                        for (int i = 0; i < types.Length; i++)
                        {
                            if (!types[i].IsClass)
                                continue;

                            // get internal static methods
                            foreach (MethodInfo mi in types[i].GetMethods(BindingFlags.Default | BindingFlags.NonPublic | BindingFlags.Static))
                            {
                                object[] attrs = mi.GetCustomAttributes(typeof(MenuItem), false);
                                for (int j = 0; j < attrs.Length; j++)
                                {
                                    MenuItem miAttr = (MenuItem)attrs[j];
                                    Debug.Log(mi.DeclaringType + "." + mi.Name + ": " + miAttr.menuItem);
                                }
                            }

                        }

                    }

                }

            }

        }



        private static bool EvalAssemblyNameFilter(string assemblyName, string[] filters)
        {
            for (int i = 0; i < filters.Length; i++)
            {
                if (assemblyName.ToLowerInvariant().StartsWith(filters[i].ToLowerInvariant()))
                    return true;
            }
            return false;
        }
        private static bool EvalAssemblyReferenceFilter(Assembly assembly, string[] filters)
        {
            // check if the filter list is empty (none can apply than)
            if (filters.Length == 0)
                return false;
            // check if the assembly itself is mentioned in the filter list
            if (filters.Contains(assembly.GetName().Name))
                return true;
            // check if any references assemblies exist (if none filter can never be met)
            AssemblyName[] refNames = assembly.GetReferencedAssemblies();
            if (refNames.Length == 0)
                return false;
            else
            {
                // check each reference if it is mentioned in the filter list
                bool hasReference = false;
                for (int rInd = 0; rInd < refNames.Length; rInd++)
                    if (filters.Contains(refNames[rInd].Name))
                    {
                        hasReference = true;
                        break;
                    }
                if (!hasReference)
                    return true;
            }

            return false;
        }


        public static string GetPath(this Transform current)
        {
            if (current.parent == null)
                return "/" + current.name;
            return current.parent.GetPath() + "/" + current.name;
        }

    }

}