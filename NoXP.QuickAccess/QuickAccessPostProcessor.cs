﻿using UnityEngine;
using System.Collections;
using UnityEditor;


namespace NoXP.QuickAccess
{
    public class QuickAccessPostProcessor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            // the QuickAccessPostProcessor does not do anything to imported/changed/deleted assets
            // it only registers changes to the asset database and set the refresh flag for the QuickAccess database
            QuickAccess.DB.NeedRefresh = true;
        }
    }
}