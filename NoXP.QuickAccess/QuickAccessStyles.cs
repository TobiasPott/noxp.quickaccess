﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    public class QuickAccessStyles
    {
        //public static readonly Color ColorItemSelected = new Color(0.866f, 0.917f, 0.305f, 0.25f);
        public static readonly Color ColorItemSelected = new Color(0.766f, 0.766f, 0.766f, 0.25f);
        //public static readonly Color ColorBorder = new Color(0, 122.0f / 255, 204.0f / 255, 1.0f);

        public static readonly Color SkinColorDark = new Color32(56, 56, 56, 255);
        public static readonly Color SkinColorLight = new Color32(194, 194, 194, 255);

        private static GUIStyle _styleItemLeft = null;
        public static GUIStyle StyleItemLeft
        {
            get
            {
                if (_styleItemLeft == null)
                {
                    _styleItemLeft = new GUIStyle(EditorStyles.toolbarButton);
                    _styleItemLeft.alignment = TextAnchor.MiddleLeft;
                    _styleItemLeft.clipping = TextClipping.Clip;
                    _styleItemLeft.richText = true;
                }
                return _styleItemLeft;
            }
        }

        private static GUIStyle _styleItemCenter = null;
        public static GUIStyle StyleItemCenter
        {
            get
            {
                if (_styleItemCenter == null)
                {
                    _styleItemCenter = new GUIStyle(EditorStyles.toolbarButton);
                    _styleItemCenter.clipping = TextClipping.Clip;
                    _styleItemCenter.richText = true;
                }
                return _styleItemCenter;
            }
        }

        private static GUIStyle _styleIconCenter = null;
        public static GUIStyle StyleIconCenter
        {
            get
            {
                if (_styleIconCenter == null)
                {
                    _styleIconCenter = new GUIStyle(EditorStyles.toolbarButton);
                    _styleIconCenter.imagePosition = ImagePosition.ImageOnly;
                    _styleIconCenter.alignment = TextAnchor.MiddleCenter;
                    _styleIconCenter.padding = new RectOffset(1, 1, 1, 1);
                }
                return _styleIconCenter;
            }
        }


        private static string EditorDefaultResources = "Editor Default Resources/";
        private static string BasePath = "Assets/Editor/NoXP.QuickAccess";
        private static string SubPath_UI = "/UI";

        private static Dictionary<QuickAccessItemType, Texture2D> _qabiTex = new Dictionary<QuickAccessItemType, Texture2D>();
        private static Dictionary<string, Texture2D> _uiTex = new Dictionary<string, Texture2D>();
        public static Texture2D UIRES_QABI_None
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.None))
                    _qabiTex.Add(QuickAccessItemType.None, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/None_64x.png"));
                return _qabiTex[QuickAccessItemType.None];
            }
        }
        public static Texture2D UIRES_QABI_Undefined
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.Undefined))
                    _qabiTex.Add(QuickAccessItemType.Undefined, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/Question_64x.png"));
                return _qabiTex[QuickAccessItemType.Undefined];
            }
        }
        public static Texture2D UIRES_QABI_Script
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.Script))
                    _qabiTex.Add(QuickAccessItemType.Script, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/Script_64x.png"));
                return _qabiTex[QuickAccessItemType.Script];
            }
        }
        public static Texture2D UIRES_QABI_GameObject
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.GameObject))
                    _qabiTex.Add(QuickAccessItemType.GameObject, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/3DScene_64x.png"));
                return _qabiTex[QuickAccessItemType.GameObject];
            }
        }
        public static Texture2D UIRES_QABI_Component
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.Component))
                    _qabiTex.Add(QuickAccessItemType.Component, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/Component_64x.png"));
                return _qabiTex[QuickAccessItemType.Component];
            }
        }
        public static Texture2D UIRES_QABI_MenuItem
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.MenuItem))
                    _qabiTex.Add(QuickAccessItemType.MenuItem, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/MenuItem_64x.png"));
                return _qabiTex[QuickAccessItemType.MenuItem];
            }
        }
        public static Texture2D UIRES_QABI_Method
        {
            get
            {
                if (!_qabiTex.ContainsKey(QuickAccessItemType.Method))
                    _qabiTex.Add(QuickAccessItemType.Method, LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/Method_64x.png"));
                return _qabiTex[QuickAccessItemType.Method];
            }
        }


        public static Texture2D UIRES_Select
        {
            get
            {
                if (!_uiTex.ContainsKey("Select"))
                    _uiTex.Add("Select", LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/Select_64x.png"));
                return _uiTex["Select"];
            }
        }
        public static Texture2D UIRES_FoldoutL
        {
            get
            {
                if (!_uiTex.ContainsKey("FoldoutL"))
                    _uiTex.Add("FoldoutL", LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/FoldoutL_64x.png"));
                return _uiTex["FoldoutL"];
            }
        }
        public static Texture2D UIRES_FoldoutR
        {
            get
            {
                if (!_uiTex.ContainsKey("FoldoutR"))
                    _uiTex.Add("FoldoutR", LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/FoldoutR_64x.png"));
                return _uiTex["FoldoutR"];
            }
        }


        private static Texture2D _builtInIcon_CSScript = null;
        public static Texture2D BuiltIn_Icon_CSScript
        {
            get
            {
                if (_builtInIcon_CSScript == null)
                    _builtInIcon_CSScript = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/CSScript_64x.png");
                return _builtInIcon_CSScript;
            }
        }

        private static Texture2D _builtInIcon_JSScript = null;
        public static Texture2D BuiltIn_Icon_JSScript
        {
            get
            {
                if (_builtInIcon_JSScript == null)
                    _builtInIcon_JSScript = LoadAssetAuto<Texture2D>(BasePath + SubPath_UI + "/JSScript_64x.png");
                return _builtInIcon_JSScript;
            }
        }


        private static Texture2D _builtInIcon_Object = null;
        public static Texture2D BuiltIn_Icon_Object
        {
            get
            {
                if (_builtInIcon_Object == null)
                    _builtInIcon_Object = AssetPreview.GetMiniTypeThumbnail(QuickAccess.TObject);
                return _builtInIcon_Object;
            }
        }


        public static T LoadAsset<T>(string path, bool editorDefaultResources = false) where T : UnityEngine.Object
        {
            if (editorDefaultResources)
                return (T)EditorGUIUtility.Load(path);
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);

        }
        public static T LoadAssetAuto<T>(string path) where T : UnityEngine.Object
        {
            if (path.StartsWith(EditorDefaultResources))
                return (T)EditorGUIUtility.Load(path.Substring(EditorDefaultResources.Length));
            else
                return AssetDatabase.LoadAssetAtPath<T>(path);
        }

    }

}