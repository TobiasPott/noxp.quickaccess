﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace NoXP.QuickAccess
{

    public enum QuickAccessItemType
    {
        None,
        Component,
        GameObject,
        Method,
        Script,
        MenuItem,
        Undefined
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public sealed class BuiltInAttribute : Attribute
    {

        private readonly string _name = string.Empty;
        private readonly string _fullName = string.Empty;
        private readonly string[] _categories = new string[] { "" };
        private readonly QuickAccessItemType _itemType = QuickAccessItemType.Undefined;
        private MethodInfo _method = null;

        public string Name
        { get { return _name; } }
        public string FullName
        { get { return _fullName; } }
        public QuickAccessItemType ItemType
        { get { return _itemType; } }
        public string[] Categories
        { get { return _categories; } }

        public MethodInfo Method
        {
            get { return _method; }
            set { _method = value; }
        }

        // This is a positional argument
        public BuiltInAttribute(string name, string fullname, QuickAccessItemType type = QuickAccessItemType.Undefined, string categories = "")
        {
            _name = name;
            _fullName = fullname;
            _itemType = type;
            if (!categories.Equals(string.Empty))
                _categories = categories.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    public class BuiltIn
    {
        // category tags for default types
        public const string CatComponent = "c";
        public const string CatGameObject = "g";
        public const string CatFunc = "f";
        public const string CatMenu = "m";
        public const string CatScript = "s";
        // extended categories
        public const string CatWindow = "w";
        public const string CatProject = "p";
        public const string Cat3DObject = "3d";
        public const string CatUI = "ui";


        public class EditorUI
        {

            private const string Cat_MenuWindow = BuiltIn.CatMenu + ";" + BuiltIn.CatWindow;
            // menu items of the "Window" menu
            #region Window-Menu
            [BuiltIn("Services (Window)", "UnityEditor.Window.Services", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Services()
            { EditorApplication.ExecuteMenuItem("Window/Services"); }

            [BuiltIn("Scene (Window)", "UnityEditor.Window.Scene", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Scene()
            { EditorApplication.ExecuteMenuItem("Window/Scene"); }

            [BuiltIn("Game (Window)", "UnityEditor.Window.Game", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Game()
            { EditorApplication.ExecuteMenuItem("Window/Game"); }

            [BuiltIn("Inspector (Window)", "UnityEditor.Window.Inspector", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Inspector()
            { EditorApplication.ExecuteMenuItem("Window/Inspector"); }

            [BuiltIn("Hierarchy (Window)", "UnityEditor.Window.Hierarchy", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Hierarchy()
            { EditorApplication.ExecuteMenuItem("Window/Hierarchy"); }

            [BuiltIn("Project (Window)", "UnityEditor.Window.Project", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Project()
            { EditorApplication.ExecuteMenuItem("Window/Project"); }

            [BuiltIn("Animation (Window)", "UnityEditor.Window.Animation", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Animation()
            { EditorApplication.ExecuteMenuItem("Window/Animation"); }

            [BuiltIn("Profiler (Window)", "UnityEditor.Window.Profiler", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Profiler()
            { EditorApplication.ExecuteMenuItem("Window/Profiler"); }

            [BuiltIn("Audio Mixer (Window)", "UnityEditor.Window.AudioMixer", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_AudioMixer()
            { EditorApplication.ExecuteMenuItem("Window/Audio Mixer"); }

            [BuiltIn("Asset Store (Window)", "UnityEditor.Window.AssetStore", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_AssetStore()
            { EditorApplication.ExecuteMenuItem("Window/Asset Store"); }

            [BuiltIn("Version Control (Window)", "UnityEditor.Window.VersionControl", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_VersionControl()
            { EditorApplication.ExecuteMenuItem("Window/Version Control"); }

            [BuiltIn("Animator (Window)", "UnityEditor.Window.Animator", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Animator()
            { EditorApplication.ExecuteMenuItem("Window/Animator"); }

            [BuiltIn("Animator Parameter (Window)", "UnityEditor.Window.AnimatorParameter", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_AnimatorParameter()
            { EditorApplication.ExecuteMenuItem("Window/Animator Parameter"); }

            [BuiltIn("Sprite Packer (Window)", "UnityEditor.Window.SpritePacker", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_SpritePacker()
            { EditorApplication.ExecuteMenuItem("Window/Sprite Packer"); }

            [BuiltIn("Editor Tests Runner (Window)", "UnityEditor.Window.EditorTestsRunner", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_EditorTestsRunner()
            { EditorApplication.ExecuteMenuItem("Window/Editor Tests Runner"); }

            [BuiltIn("Lighting (Window)", "UnityEditor.Window.Lighting", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Lighting()
            { EditorApplication.ExecuteMenuItem("Window/Lighting"); }

            [BuiltIn("Occlusion Culling (Window)", "UnityEditor.Window.OcclusionCulling", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_OcclusionCulling()
            { EditorApplication.ExecuteMenuItem("Window/Occlusion Culling"); }

            [BuiltIn("Frame Debugger (Window)", "UnityEditor.Window.FrameDebugger", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_FrameDebugger()
            { EditorApplication.ExecuteMenuItem("Window/Frame Debugger"); }

            [BuiltIn("Navigation (Window)", "UnityEditor.Window.Navigation", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Navigation()
            { EditorApplication.ExecuteMenuItem("Window/Navigation"); }

            [BuiltIn("Console (Window)", "UnityEditor.Window.Console", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_Console()
            { EditorApplication.ExecuteMenuItem("Window/Console"); }

            [BuiltIn("Collab History (Window)", "UnityEditor.Window.CollabHistory", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuWindow)]
            public static void ExecMenuItem_Window_CollabHistory()
            { EditorApplication.ExecuteMenuItem("Window/Collab History"); }

            #endregion


            #region Edit/Preferences...-Menu Item
            [BuiltIn("Preferences...", "Unity.Edit.Preferences", QuickAccessItemType.MenuItem, BuiltIn.CatMenu)]
            public static void ExecMenuItem_GameObject_UI_Button()
            {
                Assembly asm = Assembly.GetAssembly(typeof(EditorWindow));
                Type tPreferenceWindow = asm.GetType("UnityEditor.PreferencesWindow");
                MethodInfo miShowPreferenceWindow = tPreferenceWindow.GetMethod("ShowPreferencesWindow", BindingFlags.NonPublic | BindingFlags.Static);
                miShowPreferenceWindow.Invoke(null, null);
            }
            #endregion
            private const string Cat_MenuProject = BuiltIn.CatMenu + ";" + BuiltIn.CatProject;
            #region Edit/ProjectSettings-Menu
            [BuiltIn("Input Manager (Project Settings)", "UnityEditor.Edit.ProjectSettings.Input", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Input()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Input"); }

            [BuiltIn("Tags and Layers (Project Settings)", "UnityEditor.Edit.ProjectSettings.TagsAndLayers", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_TagsAndLayers()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Tags and Layers"); }

            [BuiltIn("Audio (Project Settings)", "UnityEditor.Edit.ProjectSettings.Audio", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Audio()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Audio"); }

            [BuiltIn("Time (Project Settings)", "UnityEditor.Edit.ProjectSettings.Time", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Time()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Time"); }

            [BuiltIn("Player (Project Settings)", "UnityEditor.Edit.ProjectSettings.Player", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Player()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player"); }

            [BuiltIn("Physics (Project Settings)", "UnityEditor.Edit.ProjectSettings.Physics", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Physics()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Physics"); }

            [BuiltIn("Physics 2D (Project Settings)", "UnityEditor.Edit.ProjectSettings.Physics2D", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Physics2D()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Physics 2D"); }

            [BuiltIn("Quality (Project Settings)", "UnityEditor.Edit.ProjectSettings.Quality", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Quality()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Quality"); }

            [BuiltIn("Graphics (Project Settings)", "UnityEditor.Edit.ProjectSettings.Graphics", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Graphics()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Graphics"); }

            [BuiltIn("Network (Project Settings)", "UnityEditor.Edit.ProjectSettings.Network", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Network()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Network"); }

            [BuiltIn("Editor (Project Settings)", "UnityEditor.Edit.ProjectSettings.Editor", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_Editor()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Editor"); }

            [BuiltIn("Script Execution Order (Project Settings)", "UnityEditor.Edit.ProjectSettings.ScriptExecutionOrder", QuickAccessItemType.MenuItem, EditorUI.Cat_MenuProject)]
            public static void ExecMenuItem_Edit_ProjectSettings_ScriptExecutionOrder()
            { EditorApplication.ExecuteMenuItem("Edit/Project Settings/Script Execution Order"); }

            #endregion

        }

        public class Core
        {

            private const string Cat_GameObjectCommon = BuiltIn.CatGameObject;

            #region GameObject-Menu
            [BuiltIn("Create GameObject", "Unity.Core.CreateGameObject", QuickAccessItemType.MenuItem, Core.Cat_GameObjectCommon)]
            public static void CreateGameObjectEmpty()
            {
                GameObject go = new GameObject();
                go.transform.position = SceneView.lastActiveSceneView.pivot;
                Selection.activeGameObject = go;
                Undo.RegisterCreatedObjectUndo(go, "Unity.Core.CreateGameObject");
            }

            [BuiltIn("Create GameObject (as Child)", "Unity.Core.CreateGameObjectAsChild", QuickAccessItemType.MenuItem, Core.Cat_GameObjectCommon)]
            public static void CreateGameObjectEmptyAsChild()
            {
                GameObject go = new GameObject();
                go.transform.position = SceneView.lastActiveSceneView.pivot;
                if (Selection.activeGameObject != null)
                {
                    go.transform.parent = Selection.activeGameObject.transform;
                    go.transform.localPosition = Vector3.zero;
                    Selection.activeGameObject = go;
                }
                Undo.RegisterCreatedObjectUndo(go, "Unity.Core.CreateGameObjectAsChild");
            }

            [BuiltIn("Camera", "Unity.Core.Camera", QuickAccessItemType.MenuItem, Core.Cat_GameObjectCommon)]
            public static void CreateCamera()
            { EditorApplication.ExecuteMenuItem("GameObject/Camera"); }

            [BuiltIn("Particle System", "Unity.Core.ParticleSystem", QuickAccessItemType.MenuItem, Core.Cat_GameObjectCommon)]
            public static void CreateParticleSystem()
            { EditorApplication.ExecuteMenuItem("GameObject/Particle System"); }
            #endregion

            private const string Cat_GameObject3DObject = BuiltIn.CatGameObject + ";" + BuiltIn.Cat3DObject;
            // menu items of the "GameObject/3D Object" menu
            #region GameObject/3D Object-Menu
            [BuiltIn("Cube", "Unity.GameObject.3DObject.Cube", QuickAccessItemType.GameObject, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Cube()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Cube"); }

            [BuiltIn("Sphere", "Unity.GameObject.3DObject.Sphere", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Sphere()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Sphere"); }

            [BuiltIn("Capsule", "Unity.GameObject.3DObject.Capsule", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Capsule()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Capsule"); }

            [BuiltIn("Cylinder", "Unity.GameObject.3DObject.Cylinder", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Cylinder()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Cylinder"); }

            [BuiltIn("Quad", "Unity.GameObject.3DObject.Quad", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Quad()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Quad"); }

            [BuiltIn("Plane", "Unity.GameObject.3DObject.Plane", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Plane()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Plane"); }

            [BuiltIn("Terrain", "Unity.GameObject.3DObject.Terrain", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Terrain()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Terrain"); }

            [BuiltIn("Tree", "Unity.GameObject.3DObject.Tree", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_Tree()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Tree"); }

            [BuiltIn("Wind Zone", "Unity.GameObject.3DObject.WindZone", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_WindZone()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Wind Zone"); }

            [BuiltIn("3D Text", "Unity.GameObject.3DObject.3DText", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_3DText()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/3D Text"); }

            [BuiltIn("Ragdoll...", "Unity.GameObject.3DObject.RagdollWizard", QuickAccessItemType.MenuItem, Core.Cat_GameObject3DObject)]
            public static void ExecMenuItem_GameObject_3DObject_RagdollWizard()
            { EditorApplication.ExecuteMenuItem("GameObject/3D Object/Ragdoll..."); }



            #endregion
            #region GameObject/3D Object-Menu extensions
            //[BuiltIn("Cube", "QuickAccess.Primitive.Cube", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitiveCube()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Cube");
            //}
            [BuiltIn("Cube (no collider)", "QuickAccess.Primitive.Cube.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitieCubeNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Cube.NoCollider");
            }

            //[BuiltIn("Sphere", "QuickAccess.Primitive.Sphere", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitiveSphere()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Sphere");
            //}
            [BuiltIn("Sphere (no collider)", "QuickAccess.Primitive.Sphere.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitieSphereNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Sphere.NoCollider");
            }

            //[BuiltIn("Plane", "QuickAccess.Primitive.Plane", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitivePlane()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Plane);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Plane");
            //}
            [BuiltIn("Plane (no collider)", "QuickAccess.Primitive.Plane.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitiePlaneNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Plane);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Plane.NoCollider");
            }

            //[BuiltIn("Quad", "QuickAccess.Primitive.Quad", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitiveQuad()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Quad);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Quad");
            //}
            [BuiltIn("Quad (no collider)", "QuickAccess.Primitive.Quad.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitieQuadNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Quad);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Plane.NoCollider");
            }

            //[BuiltIn("Capsule", "QuickAccess.Primitive.Capsule", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitiveCapsule()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Capsule");
            //}
            [BuiltIn("Capsule (no collider)", "QuickAccess.Primitive.Capsule.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitieCapsuleNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Plane.NoCollider");
            }

            //[BuiltIn("Cylinder", "QuickAccess.Primitive.Cylinder", QuickAccessItemType.Method, "3D")]
            //public static void CreatePrimitiveCylinder()
            //{
            //    GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            //    SetPositionSelect(go);
            //    Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Cylinder");
            //}
            [BuiltIn("Cylinder (no collider)", "QuickAccess.Primitive.Cylinder.NoCollider", QuickAccessItemType.Method, Core.Cat_GameObject3DObject)]
            public static void CreatePrimitieCylinderNoCollider()
            {
                GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                SetPositionSelectNoCollider(go);
                Undo.RegisterCreatedObjectUndo(go, "QuickAccess.Primitive.Cylinder.NoCollider");
            }

            private static void SetPositionSelectNoCollider(GameObject go)
            {
                GameObject.DestroyImmediate(go.GetComponent<Collider>());
                SetPositionSelect(go);
            }
            private static void SetPositionSelect(GameObject go)
            {
                go.transform.position = SceneView.lastActiveSceneView.pivot;
                Selection.activeGameObject = go;
            }

            #endregion


            private const string Cat_GameObjectUI = BuiltIn.CatGameObject + ";" + BuiltIn.CatUI;
            // menu items of the "GameObject/UI" menu
            #region GameObject/UI-Menu
            [BuiltIn("Text", "Unity.GameObject.UI.Text", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Text()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Text"); }

            [BuiltIn("Image", "Unity.GameObject.UI.Image", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Image()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Image"); }

            [BuiltIn("Raw Image", "Unity.GameObject.UI.RawImage", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_RawImage()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Raw Image"); }

            [BuiltIn("Button", "Unity.GameObject.UI.Button", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Button()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Button"); }

            [BuiltIn("Toggle", "Unity.GameObject.UI.Toggle", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Toggle()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Toggle"); }

            [BuiltIn("Slider", "Unity.GameObject.UI.Slider", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Slider()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Slider"); }

            [BuiltIn("Scrollbar", "Unity.GameObject.UI.Scrollbar", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Scrollbar()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Scrollbar"); }

            [BuiltIn("Dropdown", "Unity.GameObject.UI.Dropdown", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Dropdown()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Dropdown"); }

            [BuiltIn("Input Field", "Unity.GameObject.UI.InputField", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_InputField()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Input Field"); }

            [BuiltIn("Canvas", "Unity.GameObject.UI.Canvas", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Canvas()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Canvas"); }

            [BuiltIn("Panel", "Unity.GameObject.UI.Panel", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_Panel()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Panel"); }

            [BuiltIn("Scroll View", "Unity.GameObject.UI.ScrollView", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_ScrollView()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Scroll View"); }

            [BuiltIn("Event System", "Unity.GameObject.UI.EventSystem", QuickAccessItemType.MenuItem, Core.Cat_GameObjectUI)]
            public static void ExecMenuItem_GameObject_UI_EventSystem()
            { EditorApplication.ExecuteMenuItem("GameObject/UI/Event System"); }

            #endregion

        }
        

    }

}