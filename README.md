# README #

### What is this repository for? ###

* QuickAccess is an editor extension for Unity3D. It allows fast look up to built-in components, menus and fast access to prefabs and other assets in a game project.

### How do I get set up? ###

You can:

* download the full repository and build the project yourself with Visual Studio and copy the "NoXP.QuickAccess" folder from the "Output" directory inside the Visual Studio project folder.
* go to "Downloads" and grab one of the prebuild .unitypackage files and import it to your unity project. One contains the source code files and the other uses the build assembly.

Remarks:

* The tool was developed using Unity 5.4 (I assume at least a 5.x is requried but cannot say if anything special does not work in older versions of Unity)

